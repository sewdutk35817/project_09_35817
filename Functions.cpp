#include<iostream>
#include<iomanip>
#include<windows.h>
#include<conio.h>
#include<string>
#include<ctime>
#include<cstdlib>
#include <time.h>
#include <fstream>
#include "Functions.h"
#include "cClass.h"


using namespace std;


void Text_Function()
{
    Text text;
    text.print_game_title();
}

void Won_Function()
{
    Text text;
    text.print_winning_text();
}

void Lost_Function()
{
    Text text;
    text.print_losing_text();
}

void Statement(const int sign[],char board[Size][Size])
{
    cout<<endl;
    cout<<"          TWOJA TABLICA     \n";
    cout<<"      "<<" A B C D E F G H I J\n";
    for(int j=0;j<Size;j++)
    {

        printf("     %2d",sign[j]);

        for(int k=0;k<Size;k++)
        {
            cout<<board[j][k]<<" ";
        }
        cout<<endl;
    }
}

void Ship4( string orient,string x, string y,char Board_User[][Size],char Board_User_Conditions[][Size])
{
    cout<<"Ustaw statek ****\n";
    cout<<"Wybierz orientacje statku pionowa(0) czy pozioma(1):";
    cin>>orient;

    orient=orient+' ';
    if( (int)(orient[1])!=(int)(' ')){orient="2";}

    while( (int)(orient[0])!=48 && (int)(orient[0])!=49 )
    {
        cout<<"Wybierz orientacje statku pionowa(0) czy pozioma(1) jeszcze raz:";
        cin>>orient;
        orient=orient+' ';
        if( (int)(orient[1])!=(int)(' ')){orient="2";}
        cout<<endl;
    }

    if(orient[0]=='0')
    {
        cout<<endl;
        cout<<"Wpisz koordynaty statku x (z przedzialu od 1 do 10) i y (z przedzialu od 1 do 7)\n";
        cin>>x>>y;

        while( (x[0]>'9' || x[0]<'1') || (y[0]>'9' || y[0]<'1') || (stoi(x)<1 || stoi(x)>10 ) || ( stoi(y)<1 || stoi(y)>7 ) )
        {
            cout<<"Wpisz koordynaty statku x (z przedzialu od 1 do 10) i y (z przedzialu od 1 do 7) jeszcze raz\n";
            cin>>x>>y;

        }

        for(int y1=stoi(y)-1;y1<stoi(y)+3;y1++)
        {
            Board_User[y1][( stoi(x)-1 )]='*';
        }

        //Ustawianie Tablicy zabezpieczajacej polozenie statkow
        {
            if( (stoi(x)-2)>=0 && (stoi(x)-2)<10)
            {
                if( (stoi(y)-2)>=0 && (stoi(y)-2)<10)
                {
                    Board_User_Conditions[stoi(y)-2][( stoi(x)-2 )]='o';
                }

                Board_User_Conditions[stoi(y)-1][( stoi(x)-2 )]='o';
                Board_User_Conditions[stoi(y) ][( stoi(x)-2 )]='o';
                Board_User_Conditions[stoi(y)+1][( stoi(x)-2 )]='o';
                Board_User_Conditions[stoi(y)+2][( stoi(x)-2 )]='o';

                if( (stoi(y)+3)>=0 && (stoi(y)+3)<10)
                {
                    Board_User_Conditions[stoi(y)+3][( stoi(x)-2 )]='o';
                }
            }

            if( (stoi(x)-1)>=0 && (stoi(x)-1)<10)
            {
                if( (stoi(y)-2)>=0 && (stoi(y)-2)<10)
                {
                    Board_User_Conditions[stoi(y)-2][( stoi(x)-1 )]='o';
                }

                Board_User_Conditions[stoi(y)-1][( stoi(x)-1 )]='o';
                Board_User_Conditions[stoi(y) ][( stoi(x)-1 )]='o';
                Board_User_Conditions[stoi(y)+1][( stoi(x)-1 )]='o';
                Board_User_Conditions[stoi(y)+2][( stoi(x)-1 )]='o';

                if( (stoi(y)+3)>=0 && (stoi(y)+3)<10)
                {
                    Board_User_Conditions[stoi(y)+3][( stoi(x)-1 )]='o';
                }
            }

            if( (stoi(x))>=0 && (stoi(x))<10)
            {
                if( (stoi(y)-2)>=0 && (stoi(y)-2)<10)
                {
                    Board_User_Conditions[stoi(y)-2][( stoi(x) )]='o';
                }

                Board_User_Conditions[stoi(y)-1][( stoi(x) )]='o';
                Board_User_Conditions[stoi(y) ][( stoi(x) )]='o';
                Board_User_Conditions[stoi(y)+1][( stoi(x) )]='o';
                Board_User_Conditions[stoi(y)+2][( stoi(x) )]='o';

                if( (stoi(y)+3)>=0 && (stoi(y)+3)<10)
                {
                    Board_User_Conditions[stoi(y)+3][( stoi(x) )]='o';
                }
            }
        }



    }

    else if(orient[0]=='1')
    {
        cout<<endl;
        cout<<"Wpisz koordynaty statku x (z przedzialu od 1 do 7) i y (z przedzialu od 1 do 10)\n";
        cin>>x>>y;

        while( (x[0]>'9' || x[0]<'1') || (y[0]>'9' || y[0]<'1') || (stoi(x)<1 || stoi(x)>7 ) || ( stoi(y)<1 || stoi(y)>10 ) )
        {
            cout<<"Wpisz koordynaty statku x (z przedzialu od 1 do 7) i y (z przedzialu od 1 do 10) jeszcze raz\n";
            cin>>x>>y;

        }

        for(int x1=stoi(x)-1;x1<stoi(x)+3;x1++)
        {
            Board_User[ ( stoi(y)-1 ) ][x1]='*';
        }

        //Ustawianie Tablicy zabezpieczajacej polozenie statkow
        {
            if( (stoi(y)-2)>=0 && (stoi(y)-2)<10)
            {
                if( (stoi(x)-2)>=0 && (stoi(x)-2)<10)
                {
                    Board_User_Conditions[ ( stoi(y)-2 ) ][(stoi(x)-2)]='o';
                }

                Board_User_Conditions[ ( stoi(y)-2 ) ][(stoi(x)-1)]='o';
                Board_User_Conditions[ ( stoi(y)-2 ) ][(stoi(x)-0)]='o';
                Board_User_Conditions[ ( stoi(y)-2 ) ][(stoi(x)+1)]='o';
                Board_User_Conditions[ ( stoi(y)-2 ) ][(stoi(x)+2)]='o';

                if( (stoi(x)+3)>=0 && (stoi(x)+3)<10)
                {
                    Board_User_Conditions[ ( stoi(y)-2 ) ][(stoi(x)+3)]='o';
                }

            }

            if( (stoi(y)-1)>=0 && (stoi(y)-1)<10)
            {
                if( (stoi(x)-2)>=0 && (stoi(x)-2)<10)
                {
                    Board_User_Conditions[ ( stoi(y)-1 ) ][(stoi(x)-2)]='o';
                }

                Board_User_Conditions[ ( stoi(y)-1 ) ][(stoi(x)-1)]='o';
                Board_User_Conditions[ ( stoi(y)-1 ) ][(stoi(x)-0)]='o';
                Board_User_Conditions[ ( stoi(y)-1 ) ][(stoi(x)+1)]='o';
                Board_User_Conditions[ ( stoi(y)-1 ) ][(stoi(x)+2)]='o';

                if( (stoi(x)+3)>=0 && (stoi(x)+3)<10)
                {
                    Board_User_Conditions[ ( stoi(y)-1 ) ][(stoi(x)+3)]='o';
                }

            }

            if( (stoi(y))>=0 && (stoi(y))<10)
            {
                if( (stoi(x)-2)>=0 && (stoi(x)-2)<10)
                {
                    Board_User_Conditions[ ( stoi(y) ) ][(stoi(x)-2)]='o';
                }

                Board_User_Conditions[ ( stoi(y) ) ][(stoi(x)-1)]='o';
                Board_User_Conditions[ ( stoi(y) ) ][(stoi(x)-0)]='o';
                Board_User_Conditions[ ( stoi(y) ) ][(stoi(x)+1)]='o';
                Board_User_Conditions[ ( stoi(y) ) ][(stoi(x)+2)]='o';

                if( (stoi(x)+3)>=0 && (stoi(x)+3)<10)
                {
                    Board_User_Conditions[ ( stoi(y) ) ][(stoi(x)+3)]='o';
                }

            }
        }
    }
}

void Ship3( string orient,string x, string y,char Board_User[][Size],char Board_User_Conditions[][Size])
{
    cout<<"Ustaw statek ***\n";
    cout<<"Wybierz orientacje statku pionowa(0) czy pozioma(1):";
    cin>>orient;

    orient=orient+' ';
    if( (int)(orient[1])!=(int)(' ')){orient="2";}

    while( (int)(orient[0])!=48 && (int)(orient[0])!=49 )
    {
        cout<<"Wybierz orientacje statku pionowa(0) czy pozioma(1) jeszcze raz:";
        cin>>orient;
        orient=orient+' ';
        if( (int)(orient[1])!=(int)(' ')){orient="2";}
        cout<<endl;
    }


    if(orient[0]=='0')
    {
        cout<<endl;

        while( (x[0]>'9' || x[0]<'1') || (y[0]>'9' || y[0]<'1') || (stoi(x)<1 || stoi(x)>10 ) || ( stoi(y)<1 || stoi(y)>8 ) )
        {
            cout<<"Wpisz koordynaty statku x (z przedzialu od 1 do 10) i y (z przedzialu od 1 do 8) \n";
            cin>>x>>y;
            if(Board_User_Conditions[stoi(y)-1][stoi(x)-1]=='o'){x='0';}
            if(Board_User_Conditions[stoi(y)-0][stoi(x)-1]=='o'){x='0';}
            if(Board_User_Conditions[stoi(y)+1][stoi(x)-1]=='o'){x='0';}
        }


        for(int y1=stoi(y)-1;y1<stoi(y)+2;y1++)
        {
            Board_User[y1][ ( stoi(x)-1 )]='*';
        }

        //Ustawianie Tablicy zabezpieczajacej polozenie statkow
        {
            if( (stoi(x)-2)>=0 && (stoi(x)-2)<10)
            {
                if( (stoi(y)-2)>=0 && (stoi(y)-2)<10)
                {
                    Board_User_Conditions[stoi(y)-2][( stoi(x)-2 )]='o';
                }

                Board_User_Conditions[stoi(y)-1][( stoi(x)-2 )]='o';
                Board_User_Conditions[stoi(y) ][( stoi(x)-2 )]='o';
                Board_User_Conditions[stoi(y)+1][( stoi(x)-2 )]='o';

                if( (stoi(y)+2)>=0 && (stoi(y)+2)<10)
                {
                    Board_User_Conditions[stoi(y)+2][( stoi(x)-2 )]='o';
                }
            }

            if( (stoi(x)-1)>=0 && (stoi(x)-1)<10)
            {
                if( (stoi(y)-2)>=0 && (stoi(y)-2)<10)
                {
                    Board_User_Conditions[stoi(y)-2][( stoi(x)-1 )]='o';
                }

                Board_User_Conditions[stoi(y)-1][( stoi(x)-1 )]='o';
                Board_User_Conditions[stoi(y) ][( stoi(x)-1 )]='o';
                Board_User_Conditions[stoi(y)+1][( stoi(x)-1 )]='o';

                if( (stoi(y)+2)>=0 && (stoi(y)+2)<10)
                {
                    Board_User_Conditions[stoi(y)+2][( stoi(x)-1 )]='o';
                }
            }

            if( (stoi(x))>=0 && (stoi(x))<10)
            {
                if( (stoi(y)-2)>=0 && (stoi(y)-2)<10)
                {
                    Board_User_Conditions[stoi(y)-2][( stoi(x) )]='o';
                }

                Board_User_Conditions[stoi(y)-1][( stoi(x) )]='o';
                Board_User_Conditions[stoi(y) ][( stoi(x) )]='o';
                Board_User_Conditions[stoi(y)+1][( stoi(x) )]='o';

                if( (stoi(y)+2)>=0 && (stoi(y)+2)<10)
                {
                    Board_User_Conditions[stoi(y)+2][( stoi(x) )]='o';
                }
            }

        }

    }

    else if(orient[0]=='1')
    {
        cout<<endl;

        while( (x[0]>'9' || x[0]<'1') || (y[0]>'9' || y[0]<'1') || (stoi(x)<1 || stoi(x)>8 ) || ( stoi(y)<1 || stoi(y)>10 ) )
        {
            cout<<"Wpisz koordynaty statku x (z przedzialu od 1 do 8) i y (z przedzialu od 1 do 10)\n";
            cin>>x>>y;
            if(Board_User_Conditions[stoi(y)-1][stoi(x)-1]=='o'){x='0';}
            if(Board_User_Conditions[stoi(y)-1][stoi(x)-0]=='o'){x='0';}
            if(Board_User_Conditions[stoi(y)-1][stoi(x)+1]=='o'){x='0';}
        }

        for(int x1=stoi(x)-1;x1<stoi(x)+2;x1++)
        {
            Board_User[( stoi(y)-1 )][x1]='*';
        }

        //Ustawianie Tablicy zabezpieczajacej polozenie statkow
        {
            if( (stoi(y)-2)>=0 && (stoi(y)-2)<10)
            {
                if( (stoi(x)-2)>=0 && (stoi(x)-2)<10)
                {
                    Board_User_Conditions[stoi(y)-2][( stoi(x)-2 )]='o';
                }

                Board_User_Conditions[stoi(y)-2][( stoi(x)-1 )]='o';
                Board_User_Conditions[stoi(y)-2][( stoi(x)-0 )]='o';
                Board_User_Conditions[stoi(y)-2][( stoi(x)+1 )]='o';

                if( (stoi(x)+2)>=0 && (stoi(x)+2)<10)
                {
                    Board_User_Conditions[stoi(y)-2][( stoi(x)+2 )]='o';
                }
            }

            if( (stoi(y)-1)>=0 && (stoi(y)-1)<10)
            {
                if( (stoi(x)-2)>=0 && (stoi(x)-2)<10)
                {
                    Board_User_Conditions[stoi(y)-1][( stoi(x)-2 )]='o';
                }

                Board_User_Conditions[stoi(y)-1][( stoi(x)-1 )]='o';
                Board_User_Conditions[stoi(y)-1][( stoi(x)-0 )]='o';
                Board_User_Conditions[stoi(y)-1][( stoi(x)+1 )]='o';

                if( (stoi(x)+2)>=0 && (stoi(x)+2)<10)
                {
                    Board_User_Conditions[stoi(y)-1][( stoi(x)+2 )]='o';
                }
            }

            if( (stoi(y))>=0 && (stoi(y))<10)
            {
                if( (stoi(x)-2)>=0 && (stoi(x)-2)<10)
                {
                    Board_User_Conditions[stoi(y) ][( stoi(x)-2 )]='o';
                }

                Board_User_Conditions[stoi(y) ][( stoi(x)-1 )]='o';
                Board_User_Conditions[stoi(y) ][( stoi(x)-0 )]='o';
                Board_User_Conditions[stoi(y) ][( stoi(x)+1 )]='o';

                if( (stoi(x)+2)>=0 && (stoi(x)+2)<10)
                {
                    Board_User_Conditions[stoi(y) ][( stoi(x)+2 )]='o';
                }
            }

        }
    }
}

void Ship2( string orient,string x, string y,char Board_User[][Size],char Board_User_Conditions[][Size])
{
    cout<<"Ustaw statek **\n";
    cout<<"Wybierz orientacje statku pionowa(0) czy pozioma(1):";
    cin>>orient;
    orient=orient+' ';
    if( (int)(orient[1])!=(int)(' ')){orient="2";}

    while( (int)(orient[0])!=48 && (int)(orient[0])!=49 )
    {
        cout<<"Wybierz orientacje statku pionowa(0) czy pozioma(1) jeszcze raz:";
        cin>>orient;
        orient=orient+' ';
        if( (int)(orient[1])!=(int)(' ')){orient="2";}
        cout<<endl;
    }


    if(orient[0]=='0')
    {
        cout<<endl;

        while( (x[0]>'9' || x[0]<'1') || (y[0]>'9' || y[0]<'1') || (stoi(x)<1 || stoi(x)>10 ) || ( stoi(y)<1 || stoi(y)>9  ))
        {
            cout<<"Wpisz koordynaty statku x (z przedzialu od 1 do 10) i y (z przedzialu od 1 do 9)\n";
            cin>>x>>y;
            if(Board_User_Conditions[stoi(y)-1][stoi(x)-1]=='o'){x='0';}
            if(Board_User_Conditions[stoi(y)][stoi(x)-1]=='o'){x='0';}
        }


        for(int y1=stoi(y)-1;y1<stoi(y)+1;y1++)
        {
            Board_User[y1][(stoi(x)-1)]='*';
        }

        //Ustawianie Tablicy zabezpieczajacej polozenie statkow
        {
            if( (stoi(x)-2)>=0 && (stoi(x)-2)<10)
            {
                if( (stoi(y)-2)>=0 && (stoi(y)-2)<10)
                {
                    Board_User_Conditions[stoi(y)-2][( stoi(x)-2 )]='o';
                }

                Board_User_Conditions[stoi(y)-1][( stoi(x)-2 )]='o';
                Board_User_Conditions[stoi(y) ][( stoi(x)-2 )]='o';

                if( (stoi(y)+1)>=0 && (stoi(y)+1)<10)
                {
                    Board_User_Conditions[stoi(y)+1][( stoi(x)-2 )]='o';
                }
            }

            if( (stoi(x)-1)>=0 && (stoi(x)-1)<10)
            {
                if( (stoi(y)-2)>=0 && (stoi(y)-2)<10)
                {
                    Board_User_Conditions[stoi(y)-2][( stoi(x)-1 )]='o';
                }

                Board_User_Conditions[stoi(y)-1][( stoi(x)-1 )]='o';
                Board_User_Conditions[stoi(y) ][( stoi(x)-1 )]='o';

                if( (stoi(y)+1)>=0 && (stoi(y)+1)<10)
                {
                    Board_User_Conditions[stoi(y)+1][( stoi(x)-1 )]='o';
                }
            }

            if( (stoi(x))>=0 && (stoi(x))<10)
            {
                if( (stoi(y)-2)>=0 && (stoi(y)-2)<10)
                {
                    Board_User_Conditions[stoi(y)-2][( stoi(x) )]='o';
                }

                Board_User_Conditions[stoi(y)-1][( stoi(x) )]='o';
                Board_User_Conditions[stoi(y) ][( stoi(x) )]='o';

                if( (stoi(y)+1)>=0 && (stoi(y)+1)<10)
                {
                    Board_User_Conditions[stoi(y)+1][( stoi(x) )]='o';
                }
            }

        }
    }
    else if(orient[0]=='1')
    {
        cout<<endl;

        while( (x[0]>'9' || x[0]<'1') || (y[0]>'9' || y[0]<'1') || (stoi(x)<1 || stoi(x)>9 ) || ( stoi(y)<1 || stoi(y)>10 ) )
        {
            cout<<"Wpisz koordynaty statku x (z przedzialu od 1 do 9) i y (z przedzialu od 1 do 10)\n";
            cin>>x>>y;
            if(Board_User_Conditions[stoi(y)-1][stoi(x)-1]=='o'){x='0';}
            if(Board_User_Conditions[stoi(y)-1][stoi(x)-0]=='o'){x='0';}
        }


        for(int x1=stoi(x)-1;x1<stoi(x)+1;x1++)
        {
            Board_User[(stoi(y)-1)][x1]='*';
        }

        //Ustawianie Tablicy zabezpieczajacej polozenie statkow
        {
            if( (stoi(y)-2)>=0 && (stoi(y)-2)<10)
            {
                if( (stoi(x)-2)>=0 && (stoi(x)-2)<10)
                {
                    Board_User_Conditions[stoi(y)-2][( stoi(x)-2 )]='o';
                }

                Board_User_Conditions[stoi(y)-2][( stoi(x)-1 )]='o';
                Board_User_Conditions[stoi(y)-2][( stoi(x)-0 )]='o';

                if( (stoi(x)+1)>=0 && (stoi(x)+1)<10)
                {
                    Board_User_Conditions[stoi(y)-2][( stoi(x)+1 )]='o';
                }
            }

            if( (stoi(y)-1)>=0 && (stoi(y)-1)<10)
            {
                if( (stoi(x)-2)>=0 && (stoi(x)-2)<10)
                {
                    Board_User_Conditions[stoi(y)-1][( stoi(x)-2 )]='o';
                }

                Board_User_Conditions[stoi(y)-1][( stoi(x)-1 )]='o';
                Board_User_Conditions[stoi(y)-1][( stoi(x)-0 )]='o';

                if( (stoi(x)+1)>=0 && (stoi(x)+1)<10)
                {
                    Board_User_Conditions[stoi(y)-1][( stoi(x)+1 )]='o';
                }
            }

            if( (stoi(y))>=0 && (stoi(y))<10)
            {
                if( (stoi(x)-2)>=0 && (stoi(x)-2)<10)
                {
                    Board_User_Conditions[stoi(y) ][( stoi(x)-2 )]='o';
                }

                Board_User_Conditions[stoi(y) ][( stoi(x)-1 )]='o';
                Board_User_Conditions[stoi(y) ][( stoi(x)-0 )]='o';

                if( (stoi(x)+1)>=0 && (stoi(x)+1)<10)
                {
                    Board_User_Conditions[stoi(y) ][( stoi(x)+1 )]='o';
                }
            }

        }
    }

}

void Ship1(string x, string y,char Board_User[][Size],char Board_User_Conditions[][Size])
{
    cout<<"Ustaw statek *\n";

    cout<<endl;

    while( (x[0]>'9' || x[0]<'1') || (y[0]>'9' || y[0]<'1') || (stoi(x)<1 || stoi(x)>10 ) || ( stoi(y)<1 || stoi(y)>10 ) )
    {
        cout<<"Wpisz koordynaty statku x (z przedzialu od 1 do 10) i y (z przedzialu od 1 do 10)\n";
        cin>>x>>y;

        if( (int)('9')<(int)( x[0]) || (int)( x[0])<(int)('0') )
        {x="1"+x;}

        if( (int)('9')<(int)( y[0]) || (int)( x[0])<(int)('0') )
        {y="1"+y;}

        else if(stoi(y)-1<=9&&stoi(y)-1>=0)
        {

            if(stoi(x)-1<=9&&stoi(x)-1>=0)
            {
                if(Board_User_Conditions[stoi(y)-1][stoi(x)-1]=='o'){x='0';}
            }

            if(Board_User_Conditions[stoi(y)-1][stoi(x)-0]=='o'){x='0';}

            if(stoi(x)+1<=9&&stoi(x)+1>=0)
            {
                if(Board_User_Conditions[stoi(y)-1][stoi(x)+1]=='o'){x='0';}
            }

        }



        if(stoi(x)-1<=9&&stoi(x)-1>=0)
        {
            if(Board_User_Conditions[stoi(y)][stoi(x)-1]=='o'){x='0';}
        }

        if(Board_User_Conditions[stoi(y)][stoi(x)]=='o'){x='0';}

        if(stoi(x)+1<=9&&stoi(x)+1>=0)
        {
            if(Board_User_Conditions[stoi(y)][stoi(x)+1]=='o'){x='0';}
        }


        else if(stoi(y)+1<=9&&stoi(y)+1>=0)
        {

            if(stoi(x)-1<=9&&stoi(x)-1>=0)
            {
                if(Board_User_Conditions[stoi(y)+1][stoi(x)-1]=='o'){x='0';}
            }

            if(Board_User_Conditions[stoi(y)+1][stoi(x)]=='o'){x='0';}

            if(stoi(x)+1<=9&&stoi(x)+1>=0)
            {
                if(Board_User_Conditions[stoi(y)+1][stoi(x)+1]=='o'){x='0';}
            }

        }

    }

    Board_User[stoi(y)-1][(stoi(x)-1)]='*';

    //Ustawianie Tablicy zabezpieczajacej polozenie statkow
    {
        if( (stoi(x)-2)>=0 && (stoi(x)-2)<10)
        {
            if( (stoi(y)-2)>=0 && (stoi(y)-2)<10)
            {
                Board_User_Conditions[stoi(y)-2][( stoi(x)-2 )]='o';
            }

            Board_User_Conditions[stoi(y)-1][( stoi(x)-2 )]='o';

            if( (stoi(y)+1)>=0 && (stoi(y)+1)<10)
            {
                Board_User_Conditions[stoi(y)+0][( stoi(x)-2 )]='o';
            }
        }

        if( (stoi(x)-1)>=0 && (stoi(x)-1)<10)
        {
            if( (stoi(y)-2)>=0 && (stoi(y)-2)<10)
            {
                Board_User_Conditions[stoi(y)-2][( stoi(x)-1 )]='o';
            }

            Board_User_Conditions[stoi(y)-1][( stoi(x)-1 )]='o';

            if( (stoi(y)+1)>=0 && (stoi(y)+1)<10)
            {
                Board_User_Conditions[stoi(y)+0][( stoi(x)-1 )]='o';
            }
        }

        if( (stoi(x))>=0 && (stoi(x))<10)
        {
            if( (stoi(y)-2)>=0 && (stoi(y)-2)<10)
            {
                Board_User_Conditions[stoi(y)-2][( stoi(x) )]='o';
            }

            Board_User_Conditions[stoi(y)-1][( stoi(x) )]='o';

            if( (stoi(y)+1)>=0 && (stoi(y)+1)<10)
            {
                Board_User_Conditions[stoi(y)+0][( stoi(x) )]='o';
            }
        }

    }
}

void Shots(char Board_Computer[Size][Size],int quantity_of_pools_computer,char Shooting_Board_User[Size][Size],char Board_User[Size][Size],int quantity_of_pools_user,char Shooting_Board_Computer[Size][Size])
{
    string x="0",y="0";
    int xk=0,yk=0;

    while( quantity_of_pools_computer>0 && quantity_of_pools_user>0)
    {

        while( ( x[0]>'9' || x[0]<'1') || (y[0]>'9' || y[0]<'1') || (stoi(x)<1 || stoi(x)>10 ) || ( stoi(y)<1 || stoi(y)>10 ) )
        {

            cout<<"Wpisz koordynaty statku do strzalu x (z przedzialu od 1 do 10) i y (z przedzialu od 1 do 10)\n";
            cin>>x>>y;

            {
                if( (int)('9')<(int)( x[0]) || (int)( x[0])<(int)('0') )
                {x="0"+x;}

                if( (int)('9')<(int)( y[0]) || (int)( x[0])<(int)('0') )
                {y="0"+y;}
            }


            if(Shooting_Board_User[stoi(y)-1][stoi(x)-1]=='o'|| Shooting_Board_User[stoi(y)-1][stoi(x)-1]=='*')
            {
                x="0";
            }
            else if(Board_Computer[stoi(y)-1][stoi(x)-1]=='*')
            {
                quantity_of_pools_computer--;
                Shooting_Board_User[stoi(y)-1][stoi(x)-1]='X';

                cout<<"          Trafiles          \n";
                cout<<endl;
                cout<<"      "<<" A B C D E F G H I J\n";
                for(int i=0;i<Size;i++)
                {
                    printf("     %2d",i+1);
                    for(int j=0;j<Size;j++)
                    {

                        cout<<Shooting_Board_User[i][j]<<" ";

                    }
                    cout<<endl;
                }
                x="0";

            }

            else if(Board_Computer[stoi(y)-1][stoi(x)-1]!='*')
            {
                Shooting_Board_User[stoi(y)-1][stoi(x)-1]='o';
                cout<<"         Nie Trafiles       \n";
                cout<<endl;
            }

        }

        xk=0,yk=0;

        while( (xk<1 || xk>10 ) || ( yk<1 || yk>10 ) )
        {
            xk=rand()%10+1;
            yk=rand()%10+1;

            if(Shooting_Board_Computer[yk][xk]=='o'|| Shooting_Board_Computer[yk][xk]=='X')
            {
                xk=0;
            }
            else if(Board_User[yk][xk]=='*')
            {
                quantity_of_pools_user--;

                Shooting_Board_Computer[yk][xk]='X';
                cout<<"      Komputer  Trafil      \n";
                cout<<endl;
                xk=0;

            }

            else if(Board_User[yk][xk]!='*')
            {
                Shooting_Board_Computer[yk][xk]='o';
                cout<<"     Komputer Nie Trafil      \n";
                cout<<endl;
            }

        }


        //Strzały uzytkownika i komputera
        cout<<"     Twoja tablica strzalow"<<"    Tablica strzalow komputera\n";
        cout<<"      "<<" A B C D E F G H I J"<<"      "<<" A B C D E F G H I J\n";
        for(int i=0;i<Size;i++)
        {
            printf("     %2d",i+1);
            for(int j=0;j<Size;j++)
            {

                cout<<Shooting_Board_User[i][j]<<" ";

            }
            printf("     %2d",i+1);
            for(int j=0;j<Size;j++)
            {

                cout<< Shooting_Board_Computer[i][j]<<" ";

            }
            cout<<endl;
        }
        x="0",y="0";

        cout<<endl;


        cin.ignore();
        while (1)
        {
            cout<<"Nacisnij enter aby kontynuowac\n";
            if (cin.get() == '\n')
            {

                break;
            }
        }


        system("cls");
        Text_Function();

        Statement(sign,Board_User);

        //Strzały uzytkownika i komputera
        cout<<"    Twoja tablica strzalow"<<" "<<"    Tablica strzalow komputera\n";
        cout<<"      "<<" A B C D E F G H I J"<<"      "<<" A B C D E F G H I J\n";
        for(int i=0;i<Size;i++)
        {
            printf("     %2d",i+1);
            for(int j=0;j<Size;j++)
            {

                cout<<Shooting_Board_User[i][j]<<" ";

            }
            printf("     %2d",i+1);
            for(int j=0;j<Size;j++)
            {

                cout<<Shooting_Board_Computer[i][j]<<" ";

            }
            cout<<endl;
        }
    }

    if(quantity_of_pools_computer==0)
    {
        system("cls");
        Won_Function();
    }

    else
    {
        system("cls");
        Lost_Function();
    }
}


