#ifndef FUNCTIONS_H
#define FUNCTIONS_H
#define Size 10
#include <iostream>


const int sign[]={1,2,3,4,5,6,7,8,9,10};


void Text_Function();

void Won_Function();

void Lost_Function();

void Statement(const int sign[],char board[Size][Size]);


void Ship4( std::string orient,std::string x, std::string y,char Board_User[][Size],char Board_User_Conditions[][Size]);

void Ship3( std::string orient,std::string x, std::string y,char Board_User[][Size],char Board_User_Conditions[][Size]);

void Ship2( std::string orient,std::string x, std::string y,char Board_User[][Size],char Board_User_Conditions[][Size]);

void Ship1(std::string x, std::string y,char Board_User[][Size],char Board_User_Conditions[][Size]);

void Shots(char Board_Computer[Size][Size],int quantity_of_pools_computer,char Shooting_Board_User[Size][Size],char Board_User[Size][Size],int quantity_of_pools_user,char Shooting_Board_Computer[Size][Size]);

#endif // FUNCTIONS_H

