# Projekt NISP_09 Battleships

# Opis funkcjonalności
  Użytkownik ma możliwość ustawiania statków cztero trzy dwu i jedno polowych w ilościach kolejno 1 ,2 ,3 ,4 . 
  <br>
  Następnie losowane jest ustawienie tego samego rodzaju statków dla komputera.
  <br>
  Użytkownik i komputer grają na przemian próbując zestrzelić swoje statki.
  <br>
  Jeśli któryś z nich trafi to ma kolejną turę na oddanie strzału.
  <br>
  Jeżeli jedna ze stron straci wszystkie statki przerywamy grę.
  <br>
  W zależności od wyniku otrzymujemy napis informujący o zwycięstwie/porażce.
## Narzędzia do wytwarzania oprogramowania z których korzystaliśmy 
  Qt Creator w wersji 6.2.0 MinGw 64-bit
  <br>
  CodeBlocks w wersji 20.03 MinGw 64-bit
  <br>
  Git
  <br>
  Podczas testów jednym z większych problemów był błąd:
  <br>
  {
  <br>
  terminate called after throwing an instance of 'std::invalid_argument'
  <br>
  what():  stoi
  <br>
  }
  <br>
  Chcieliśmy aby użytkownik miał możliwość wpisania czegokolwiek jako koordanatu statku i jego pozycji.
  <br>
  Następnie otrzymałby powiadomienie o błednych współrzędnych i żeby wpisał poprawne koordynaty
  
## Podział prac
  |Funkcjonalność                                                          | Autor             |
  |------------------------------------------------------------------------|-------------------|  
  |Utworzenie pustych plików main:                                         | Michał Ciurej     |
  |Utworzenie pozostałych pustych plików:                                  | Seweryn Dutka     |
  |Utowrzenie napisów Statki,Wygrana,Przegrana                             | Seweryn Dutka     |
  |Utworzenie kodu odpowiadającego za ustawienie statków użytkownika:      | Seweryn Dutka     |
  |Utworzenie kodu odpowiadającego za ustawienie statków komputera:        | Michał Ciurej     |
  |Utworzenie kodu odpowiadającego za system strzelania:                   | Michał Ciurej     |

  Okazjonalne poprawki były wykonywane naprzemiennie przez obydwu autorów.                 

## Autorzy
Seweryn Dutka i Michał Ciurej

## Numery albumów
    35817     i    35193
