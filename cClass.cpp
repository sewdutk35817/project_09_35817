#include "cClass.h"
#include <iostream>
#include <windows.h>
#include<iomanip>

void Text::print_game_title()
{
    system("Color 3F");
    cout<<endl<<endl;
    for (int i = 0; i < title_col; i++)
    {
        cout << setw(33);
        for (int j = 0; j < title_row; j++)
        {
            if (game_title[i][j] == '#')
            {
                cout << (char)219;
            }
            else
            {
                cout << (char)32;
            }

        }

        cout << endl;
    }
}

void Text::print_winning_text()
{
    system("Color 3F");
    cout<<endl<<endl;
    for (int i = 0; i < title_col; i++)
    {
        cout << setw(26);
        for (int j = 0; j < title_row2; j++)
        {
            if (winning_text[i][j] == '#')
            {
                cout << (char)219;
            }
            else
            {
                cout << (char)32;
            }

        }

        cout << endl;
    }
}

void Text::print_losing_text()
{
    system("Color 3F");
    cout<<endl<<endl;
    for (int i = 0; i < title_col; i++)
    {
        cout << setw(20);
        for (int j = 0; j < title_row3; j++)
        {
            if (losing_text[i][j] == '#')
            {
                cout << (char)219;
            }
            else
            {
                cout << (char)32;
            }

        }

        cout << endl;
    }
}
